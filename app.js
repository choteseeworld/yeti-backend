var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var users = require('./routes/users');
var parems = require('./routes/parems')

app.use(cors())

var mongoose = require('./config/db')
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/users', users);
app.use('/parem', parems)

app.listen(3000, () => console.log('server run listening on port 3000'));