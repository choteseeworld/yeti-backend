var mongoose = require('mongoose')
var uri = 'mongodb://3.16.109.89:27017/yeti-db?authSource=admin';
var option = {
  user: 'admin',
  pass: 'admin',
  keepAlive: true,
  keepAliveInitialDelay: 300000,
  useNewUrlParser: true,
  auto_reconnect: true,
  socketTimeoutMS: 0,
  connectTimeoutMS: 0,
}
mongoose.set('useCreateIndex', true)
mongoose.connect(uri, option).then(() => console.log('<connection succesful>')).catch((err) => console.error(err));
module.exports = mongoose;