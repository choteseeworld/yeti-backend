var express = require('express');
var User = require('../models/UserModule');

module.exports = {
    createUser: function (req, res) {
        console.log(req.body)
        const adduser = new User(req.body);
        adduser.save(function (err) {
            if (err) {
                res.send(err);
                return;
            } else {
                res.json({
                    message: 'Create User Success'
                });
            }
        });
    },
    getUser: function (req, res, next) {
        User.find({}, function (err, user) {
            if (err) {
                console.log(err);
            } else {
                res.json(user);
            }
        });
    },
    getUserById: function (req, res) {
        const queryuser = {
            _id: req.body.id
        };
        User.findById(queryuser, function (err, user) {
            if (err) {
                console.log(err);
            } else {
                res.json(user);
            }
        });
    },
    editUser: function (req, res) {
        const edituser = {};
        edituser.username = req.body.username;
        edituser.userpassword = req.body.userpassword;
        edituser.email = req.body.email;
        const query = {
            _id: req.params.id
        }
        User.update(query, edituser, function (err) {
            if (err) {
                res.send(err);
                return;
            } else {
                res.json({
                    message: 'Update User Success'
                });
            }
        });
    },
    deleteUserById: function (req, res, next) {
        const query_user = {
            _id: req.params.id
        };
        User.remove(query_user, function (err, user) {
            if (err) {
                console.log(err);
            } else {
                res.json({
                    message: 'Delete User Success'
                });
            }
        });
    }
};