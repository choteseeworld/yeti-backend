var Parem = require('../models/ParemModule');

module.exports = {
    // signin: function (req, res) {
    //     var admin = await Parem.findOne({
    //         product_id: req.body.product_id
    //     }).exec();
    //     if (admin) {

    //     }

    // },
    createParem: function (req, res) {
        console.log(req.body)
        const addparem = new Parem(req.body);
        addparem.save(function (err, data) {
            if (err) {
                res.send(err);
                return;
            } else {
                res.json({
                    status: "success",
                    message: 'Create Parem Success',
                    data: data
                });
            }
        });
    },
    getParem: function (req, res) {
        Parem.find({}, function (err, params) {
            if (err) {
                console.log(err);
                res.json(err);
            } else {
                res.json(params);
            }
        });
    },
    getParemById: function (req, res) {
        const queryuser = {
            _id: req.params.id
        };
        Parem.findById(queryuser, function (err, params) {
            if (err) {
                console.log(err);
                res.json(err);
            } else {
                res.json(params);
            }
        });
    },
    connection: async function (req, res) {
        await Parem.find({
            product_id: req.body.product_id
        }, function (err, params) {
            if (err) {
                console.log(err);
                return res.status(404).json({
                    status: "error",
                    message: "ID not found",
                    data: err,
                })
            } else {
                console.log("Product ID : " + req.body.product_id + " connected!!")
                return res.status(200).json({
                    status: "success",
                    message: "connected",
                    data: params
                })
            }
        })
    }
};