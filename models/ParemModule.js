var mongoose = require('mongoose');

var ParemSchema = mongoose.Schema({
    product_id: {
        type: String,
        // required: true
    },
    temp: {
        type: Number,
        // required: true
    },
    oxi: {
        type: Number,
        // required: true
    },
    hrt: {                  //heart rate
        type: Number,
        // required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
}, {
    collection: 'Parem'
});

module.exports = mongoose.model('Parem', ParemSchema,"Parem");