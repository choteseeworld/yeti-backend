var express = require('express');
var router = express.Router();
var controller = require('../controllers/UserController.js');

router.get('/', controller.getUser);
router.post('/createuser', function (req, res) {
    console.log(req.body)
    controller.createUser(req, res)
});

module.exports = router;