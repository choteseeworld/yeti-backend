var express = require('express');
var router = express.Router();
var controller = require('../controllers/ParemController');

router.get('/', controller.getParem);
router.post('/createparem', function (req, res) {
    console.log(req.body);
    controller.createParem(req, res);
});
router.post('/connect', function (req, res) {
    controller.connection(req, res);
})
module.exports = router;